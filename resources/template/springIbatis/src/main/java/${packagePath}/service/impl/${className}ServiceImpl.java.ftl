package ${packName}.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ${packName}.dao.${className}DAO;
import ${packName}.model.${className};
import ${packName}.service.${className}Service;

/**
 * 
 * ${className}Service接口实现类
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Service
@Transactional
public class ${className}ServiceImpl implements ${className}Service
{
    @Autowired
    ${className}DAO ${className?uncap_first}DAO;
    
    @Override
    public void insert(${className} ${className?uncap_first})
    {
        ${className?uncap_first}DAO.insert(${className?uncap_first});
    }
    
    @Override
    public void deleteById(Long id)
    {
        ${className?uncap_first}DAO.deleteByPrimaryKey(id);
    }
    
    @Override
    public void updateById(${className} ${className?uncap_first})
    {
        ${className?uncap_first}DAO.updateByPrimaryKey(${className?uncap_first});
    }
    
    @Override
    public void saveOrUpdate(${className} ${className?uncap_first})
    {
        if (${className?uncap_first}.getId() == null)
        {
            insert(${className?uncap_first});
        }
        else
        {
            updateById(${className?uncap_first});
        }
    }
    
    @Override
    public ${className} queryById(Long id)
    {
        return ${className?uncap_first}DAO.selectByPrimaryKey(id);
    }
    
    @Override
    public List<${className}> queryAll()
    {
        return ${className?uncap_first}DAO.selectAll();
    }
}
